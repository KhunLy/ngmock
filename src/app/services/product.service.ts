import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly _productEndPoint: string = environment.api + '/products'

  constructor(
    private httpClient: HttpClient
  ) { }

  get(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this._productEndPoint);
  }

  getById(id: number) : Observable<Product>{
    return this.httpClient.get<Product>(this._productEndPoint + '/' + id);
  }

  add(product: Product) : Observable<void> {
    return this.httpClient.post<void>(this._productEndPoint, product);
  }

  delete(id: number): Observable<void> {
    return this.httpClient.delete<void>(this._productEndPoint + '/' + id);
  }

  update(id: number, product: Product): Observable<void> {
    return this.httpClient.put<void>(this._productEndPoint + '/' + id, product);
  }
  
}
