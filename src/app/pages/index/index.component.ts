import { Component, OnInit } from '@angular/core';
import { finalize, mergeMap } from 'rxjs/operators';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  isLoading: boolean = false;

  products!: Product[];

  constructor(
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.productService.get()
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(data => this.products = data);
  }

  delete(id: number) {
    this.isLoading = true;
    this.productService.delete(id)
      .pipe(
        mergeMap(() => this.productService.get()),
        finalize(() => this.isLoading = false),
      )
      .subscribe(data => this.products = data);

    // this.productService.delete(id)
    //   .subscribe(() => 
    //     this.productService.get()
    //       .subscribe(data => this.products = data)
    //   );
  }

}
