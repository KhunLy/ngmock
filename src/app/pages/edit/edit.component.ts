import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { ProductForm } from 'src/app/forms/product.form';
import { ProductService } from 'src/app/services/product.service';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  fg!: FormGroup;

  id!: number;

  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.fg = this.formBuilder.group(ProductForm);
    this.id = this.route.snapshot.params.id;
    if(this.id) {
      this.isLoading = true;
      this.productService.getById(this.id)
          .pipe(finalize(() => this.isLoading = false))
          .subscribe(data => this.fg.patchValue(data));
    }
  }

  submit() {
    if(this.fg.valid) {
      this.isLoading = true;
      if(this.id) {
        this.productService.update(this.id, this.fg.value)
          .pipe(finalize(() => this.isLoading = false))
          .subscribe(() => this.router.navigateByUrl('/index'));
      }
      else {
        this.productService.add(this.fg.value)
          .pipe(finalize(() => this.isLoading = false))
          .subscribe(() => this.router.navigateByUrl('/index'));
      }
    }
  }

}
