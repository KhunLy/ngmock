import { Validators } from "@angular/forms";

export const ProductForm = {
    'name': [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    'price': [null, [Validators.required, Validators.min(0), Validators.max(9999)]],
}