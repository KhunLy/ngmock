# Install
**npm install**

# Install Json Server
**npm i --save-dev json-server**

# Run Json Server
**json-server --watch** *path/to/json*